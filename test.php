<?php

require __DIR__.'/sha1.php';

$c = 10;

for ($i = 0; $i < $c; $i++) {
	$rand = fread($f = fopen('/dev/urandom', 'r'), mt_rand(1, 10000));
	assert(sha1($rand) === _sha1($rand));
	assert(sha1($rand, 1) === _sha1($rand, 1));
	fclose($f);
}
