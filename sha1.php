<?php

function _sha1($str, $raw = false) {

	$h0 = 0x67452301;
	$h1 = 0xefcdab89;
	$h2 = 0x98badcfe;
	$h3 = 0x10325476;
	$h4 = 0xc3d2e1f0;

	$bytes = strlen($str);
	$pad = 56 - ($bytes + 1) % 64;
	$pad = $pad >= 0 ? $pad : (64 - abs($pad));
	$str .= "\x80".str_repeat("\x00", $pad);
	$str .= pack('NN', 0, $bytes * 8);

	$chunks = str_split($str, 64);
	foreach ($chunks as $chunk) {
		$w = array_values(unpack('N16', $chunk));
		for ($i = 16; $i < 80; $i++) {
			$w[$i] = lrot($w[$i-3] ^ $w[$i-8] ^ $w[$i-14] ^ $w[$i-16], 1);
		}
		$a = $h0;
		$b = $h1;
		$c = $h2;
		$d = $h3;
		$e = $h4;
		for ($i = 0; $i < 80; $i++) {
			if ($i < 20) {
				$f = ($b & $c) | ((~ $b) & $d);
				$k = 0x5a827999;
			} elseif ($i >= 20 && $i < 40) {
				$f = $b ^ $c ^ $d;
				$k = 0x6ed9eba1;
			} elseif ($i >= 40 && $i < 60) {
				$f = ($b & $c) | ($b & $d) | ($c & $d);
				$k = 0x8f1bbcdc;
			} else {
				$f = $b ^ $c ^ $d;
				$k = 0xca62c1d6;
			}
			$temp = (lrot($a, 5) + $f + $e + $k + $w[$i]) & 0xffffffff;
			$e = $d;
			$d = $c;
			$c = lrot($b, 30);
			$b = $a;
			$a = $temp;
		}
		$h0 = ($h0 + $a) & 0xffffffff;
		$h1 = ($h1 + $b) & 0xffffffff;
		$h2 = ($h2 + $c) & 0xffffffff;
		$h3 = ($h3 + $d) & 0xffffffff;
		$h4 = ($h4 + $e) & 0xffffffff;
	}
	$hash = pack('N*', $h0, $h1, $h2, $h3, $h4);
	if (!$raw) list(, $hash) = unpack('H*', $hash);
	return $hash;
}

function lrot($a, $b) {
    return ($a << $b) | ($a >> (32 - $b)) & ~(-1 << $b);
}
